"use strict";
var exports = module.exports = {};
var config = require('../conf/config');
var fs = require('fs');

exports.get_controllers = function (callback) {
    fs.readdir(config.controllers_dir, function (err, files) {
        callback(err, files);
    });
};

exports.load_app = function(express_app){

};