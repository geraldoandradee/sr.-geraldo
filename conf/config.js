var exports = module.exports = {};
var path = require('path');
if (!process.env.ENV_APP) {
    process.env.ENV_APP = 'production';
}

exports.controllers_dir = path.normalize(__dirname + './../controllers/');

//exports.config.controllers = function(callback) {
//    var fs = require('fs');
//
//    fs.readdir('../controllers', function(err, files) {
//        console.log(files);
//    });
//};

//exports.config = require("./" + process.env.ENV_APP);
