/**
 * Created by Geraldo on 25/10/2014.
 */
var mongoose = require('mongoose');
var validators = require('../library/validators');

var Category = mongoose.model('Category',
    {
        description: String,

        created_at: Date,
        updated_at: { type: Date, default: Date.now },
        is_deleted: Boolean
    });

Category.schema.path('description').validate(validators.max_length(value, 250), 'Categoria não pode ter mais do que 250 caracteres');