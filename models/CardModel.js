/**
 * Created by Geraldo on 25/10/2014.
 */
var mongoose = require('mongoose');

var Card = mongoose.model('Card',
    {
        description: String, // card name
        card_number: String, // card number

        created_at: Date,
        updated_at: { type: Date, default: Date.now },
        is_deleted: Boolean
    });