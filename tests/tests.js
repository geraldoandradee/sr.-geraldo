var exports = module.exports = {};
var config = require('./../conf/config');
var loader = require('./../library/loader');
var assert = require('chai').assert;
var fs = require('fs');

describe('loader.js', function () {

    describe('controllers', function () {
        it('should return all controllers', function () {
            loader.get_controllers(function(err, controllers) {
                assert(Array.isArray(controllers));
                assert(err == undefined, 'Controller\'s path is wrong');

                controllers.forEach(function(controller) {
                    //var controller = controller.split('.', 1);
                    fs.exists(config.controllers_dir + controller, function(exists) {
                        assert(exists == true, 'Path controller is wrong or configured wrong');
                    });

                });
            });
        });
    });

});




